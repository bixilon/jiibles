/*
 * Jiibles
 * Copyright (C) 2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.jiibles.test

import de.bixilon.jiibles.Table
import de.bixilon.jiibles.TableStyles
import de.bixilon.jiibles.test.TestUtil.assertTableEquals
import org.junit.jupiter.api.Test

class TableTest {

    @Test
    fun `single column and row`() {
        val table = Table(arrayOf("First"))
        assertTableEquals(
            table, """
            +-------+
            | First |
            +-------+
        """
        )
    }

    @Test
    fun `two columns, one row`() {
        val table = Table(arrayOf("First", "Second"))
        assertTableEquals(
            table, """
            +-------+--------+
            | First | Second |
            +-------+--------+
        """
        )
    }

    @Test
    fun `two rows one column`() {
        val table = Table(arrayOf("First"))
        table += arrayOf("Data")
        assertTableEquals(
            table, """
            +-------+
            | First |
            +-------+
            | Data  |
            +-------+
        """
        )
    }

    @Test
    fun `two data rows one header single row`() {
        val table = Table(arrayOf("First"))
        table += arrayOf("Data")
        table += arrayOf("Next")
        assertTableEquals(
            table, """
            +-------+
            | First |
            +-------+
            | Data  |
            | Next  |
            +-------+
        """
        )
    }

    @Test
    fun `multiple lines and columns`() {
        val table = Table(arrayOf("First", "Second"))
        table += arrayOf("Data", "First")
        table += arrayOf("Next", "ABC")
        assertTableEquals(
            table, """
            +-------+--------+
            | First | Second |
            +-------+--------+
            | Data  | First  |
            | Next  |  ABC   |
            +-------+--------+
        """
        )
    }

    @Test
    fun `too much data`() {
        val table = Table(arrayOf("First", "Second"))
        table += arrayOf("Data", "First", "not")
        table += arrayOf("Next", "ABC", "here")
        assertTableEquals(
            table, """
            +-------+--------+
            | First | Second |
            +-------+--------+
            | Data  | First  |
            | Next  |  ABC   |
            +-------+--------+
        """
        )
    }

    @Test
    fun `too less data`() {
        val table = Table(arrayOf("First", "Second"))
        table += arrayOf("fill")
        table += arrayOf()
        assertTableEquals(
            table, """
            +-------+--------+
            | First | Second |
            +-------+--------+
            | fill  |        |
            |       |        |
            +-------+--------+
        """
        )
    }

    @Test
    fun `fancy table style`() {
        val table = Table(arrayOf("First", "Second"), style = TableStyles.FANCY)
        table += arrayOf("Data", "First")
        table += arrayOf("Next", "better")
        assertTableEquals(
            table, """
            ╔═══════╦════════╗
            ║ First ║ Second ║
            ╠═══════╬════════╣
            ║ Data  ║ First  ║
            ║ Next  ║ better ║
            ╚═══════╩════════╝
        """
        )
    }

    @Test
    fun `multiple lines with centered text`() {
        val table = Table(arrayOf("First"))
        table += arrayOf("LOOOOOONG")
        table += arrayOf("Data")
        table += arrayOf("1")
        println(table)
        assertTableEquals(
            table, """
            +-----------+
            |   First   |
            +-----------+
            | LOOOOOONG |
            |   Data    |
            |     1     |
            +-----------+
        """
        )
    }
}
