/*
 * Jiibles
 * Copyright (C) 2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.jiibles

object TableStyles {
    val CLASSIC = charArrayOf('+', '+', '+', '+', '+', '+', '+', '+', '+', '-', '|')
    val FANCY = charArrayOf('╠', '╔', '╚', '╣', '╗', '╝', '╬', '╦', '╩', '═', '║')

    const val LEFT_SIDE = 0
    const val LEFT_UP = 1
    const val BOTTOM_LEFT = 2
    const val RIGHT_SIDE = 3
    const val RIGHT_UP = 4
    const val BOTTOM_RIGHT = 5
    const val CENTER = 6
    const val TOP_CENTER = 7
    const val BOTTOM_CENTER = 8
    const val BORDER = 9
    const val SIDE = 10
}
