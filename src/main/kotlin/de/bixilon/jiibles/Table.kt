/*
 * Jiibles
 * Copyright (C) 2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.jiibles

import de.bixilon.jiibles.JiiblesUtil.NEW_LINE
import de.bixilon.jiibles.JiiblesUtil.cache
import de.bixilon.jiibles.JiiblesUtil.removeAnsi
import de.bixilon.jiibles.TableStyles.SIDE
import de.bixilon.jiibles.types.AnyLine
import de.bixilon.jiibles.types.StringLine

class Table(
    private val header: AnyLine,
    private val hideNull: Boolean = DEFAULT_HIDE_NULL,
    private val escapeAnsi: Boolean = DEFAULT_ESCAPE_ANSI,
    private val separateLines: Boolean = DEFAULT_SEPARATE_LINES,
    private val alignment: TextAlignment = DEFAULT_ALIGNMENT,
    private val style: CharArray = DEFAULT_STYLE,
) {
    private val entries: MutableList<AnyLine> = mutableListOf()
    private val headerCache: StringLine = header.cache(hideNull)
    private var entryCache: Array<StringLine> = emptyArray()
    private var cache: String = ""
    private var recache = true

    override fun toString(): String {
        if (cache.isNotEmpty() && !recache) {
            return cache
        }
        if (header.isEmpty()) {
            return JiiblesUtil.EMPTY_STRING
        }
        val cache = StringBuilder()
        val width = calculateWidth()
        val centerSeparator = JiiblesUtil.getSeparatorLine(width, style, top = true, bottom = true)
        val bottomSeparator = JiiblesUtil.getSeparatorLine(width, style, top = true, bottom = false)

        cache.append(JiiblesUtil.getSeparatorLine(width, style, top = false, bottom = true)).append(NEW_LINE)
        cache.append(headerCache.format(width, style)).append(NEW_LINE)
        cache.append(if (entryCache.isEmpty()) bottomSeparator else centerSeparator).append(NEW_LINE)

        for (line in entryCache) {
            cache.append(line.format(width, style)).append(NEW_LINE)
            if (separateLines) {
                cache.append(centerSeparator).append(NEW_LINE)
            }
        }
        if (entryCache.isNotEmpty()) {
            cache.append(bottomSeparator)
        }

        if (cache.endsWith('\n')) {
            cache.deleteCharAt(cache.length - 1)
        }

        this.cache = cache.toString()
        return this.cache
    }

    private fun StringLine.format(widths: IntArray, style: CharArray): StringBuilder {
        val builder = StringBuilder()
        builder.append(style[SIDE])
        for (index in widths.indices) {
            var entry = this.getOrElse(index) { "" }
            val width = widths[index]
            builder.append(' ')
            if (escapeAnsi) {
                entry = entry.removeAnsi()
            }
            val entryWidth = entry.length
            when (alignment) {
                TextAlignment.LEFT -> {
                    builder.append(entry)
                    builder.append(" ".repeat(width - entryWidth))
                }

                TextAlignment.CENTER -> {
                    builder.append(" ".repeat((width - entryWidth) / 2))
                    builder.append(entry)
                    builder.append(" ".repeat((width - entryWidth + 1) / 2))
                }

                TextAlignment.RIGHT -> {
                    builder.append(" ".repeat(width - entryWidth))
                    builder.append(entry)
                }
            }
            builder.append(' ')
            if (index < widths.size - 1)
                builder.append(style[SIDE])
        }

        builder.append(style[SIDE])

        return builder
    }

    private fun rebuildCache() {
        if (!recache) {
            return
        }

        if (entries.size != entryCache.size) {
            entryCache = Array<StringLine?>(entries.size) { null } as Array<StringLine>
        }

        for ((index, line) in entries.withIndex()) {
            entryCache[index] = line.cache(hideNull)
        }

        recache = false
    }

    private fun IntArray.updateWidth(entries: StringLine) {
        for (index in this.indices) {
            val length = if (entries.size <= index) 0 else entries[index].removeAnsi().length
            this[index] = maxOf(this[index], length)
        }
    }

    private fun calculateWidth(): IntArray {
        val width = IntArray(header.size)
        rebuildCache()

        width.updateWidth(headerCache)

        for (line in entryCache) {
            width.updateWidth(line)
        }
        return width
    }

    operator fun plusAssign(line: AnyLine) {
        entries += line
        recache = true
    }

    companion object {
        var DEFAULT_HIDE_NULL = false
        var DEFAULT_ESCAPE_ANSI = false
        var DEFAULT_SEPARATE_LINES = false
        var DEFAULT_ALIGNMENT = TextAlignment.CENTER
        var DEFAULT_STYLE = TableStyles.CLASSIC
    }
}
