# Java ascii tables

## Usage

Use the class `Table`. Straightforward.

```kotlin
val table = Table(arrayOf("Header 1", "Header 2"))
table += arrayOf("Fist column entry", "Second column entry")
table += arrayOf("Fist column entry", "Second column entry")
printlln(table)
```

### Maven

```xml
<dependency>
    <groupId>de.bixilon</groupId>
    <artifactId>jiibles</artifactId>
    <version>1.1.3</version>
</dependency>
```

### Gradle

```groovy
implementation 'de.bixilon:jiibles:1.1.3'
```

```kotlin
implementation("de.bixilon:jiibles:1.1.3")
```
