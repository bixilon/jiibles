/*
 * Jiibles
 * Copyright (C) 2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package de.bixilon.jiibles

import de.bixilon.jiibles.TableStyles.BORDER
import de.bixilon.jiibles.TableStyles.BOTTOM_LEFT
import de.bixilon.jiibles.TableStyles.LEFT_SIDE
import de.bixilon.jiibles.TableStyles.LEFT_UP
import de.bixilon.jiibles.types.AnyLine
import de.bixilon.jiibles.types.AnyString
import de.bixilon.jiibles.types.StringLine

object JiiblesUtil {
    private val ANSI_REGEX = "\\x1b\\[[\\d;]*m".toRegex()
    const val EMPTY_STRING = ""
    const val NULL_STRING = "null"
    const val NEW_LINE = '\n'

    fun String.removeAnsi(): String {
        if (ANSI_REGEX.find(this) == null) {
            return this
        }
        return ANSI_REGEX.replace(this, "")
    }

    fun AnyLine.cache(hideNull: Boolean): StringLine {
        return Array(this.size) { this[it].convertToString(hideNull) }
    }

    fun getSeparatorLine(widths: IntArray, style: CharArray, top: Boolean, bottom: Boolean): String {
        val builder = StringBuilder()
        val index =
            if (bottom) if (top) LEFT_SIDE else LEFT_UP else if (top) BOTTOM_LEFT else throw IllegalArgumentException("Top and bottom can not be empty!")
        builder.append(style[index])
        for (width in widths) {
            if (builder.length > 1) {
                builder.append(style[index + 6])
            }
            builder.append(style[BORDER].toString().repeat(width + 2)) // 2 spaces
        }
        builder.append(style[index + 3])

        return builder.toString()
    }


    private fun AnyString.convertToString(hideNull: Boolean): String {
        if (this == null) {
            if (hideNull) {
                return EMPTY_STRING
            }
            return NULL_STRING
        }
        return this.toString()
    }

    fun <T> table(elements: Collection<T>, vararg headers: AnyString, builder: (T) -> Array<Any?>?): Table {
        val table = Table(headers as AnyLine)

        for (element in elements) {
            table += builder(element) ?: continue
        }

        return table
    }
}
